package net.ins.ignite.conf;

import lombok.RequiredArgsConstructor;
import net.ins.ignite.domain.Statement;
import net.ins.ignite.domain.User;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.configuration.MemoryConfiguration;
import org.apache.ignite.internal.util.IgniteUtils;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.TcpDiscoveryIpFinder;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;
import org.apache.ignite.spi.discovery.tcp.ipfinder.zk.TcpDiscoveryZookeeperIpFinder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static java.lang.String.format;
import static org.apache.ignite.cache.CacheAtomicityMode.TRANSACTIONAL;
import static org.apache.ignite.cache.CacheMode.PARTITIONED;

@Configuration
@Profile("!test")
@EnableConfigurationProperties(SampleProperties.class)
@RequiredArgsConstructor
public class IgniteConfig {

    private final SampleProperties properties;

    @Profile("zookeeper")
    @Bean
    public TcpDiscoveryIpFinder zookeeperIpFinder(@Value("${ZK_HOST}") String zkHost,
                                                  @Value("${ZK_PORT}") int zkPort) {
        TcpDiscoveryZookeeperIpFinder tcpDiscoveryZookeeperIpFinder = new TcpDiscoveryZookeeperIpFinder();
        tcpDiscoveryZookeeperIpFinder.setZkConnectionString(format("%s:%s", zkHost, zkPort));
        return tcpDiscoveryZookeeperIpFinder;
    }

    @Profile("multicast")
    @Bean
    public TcpDiscoveryIpFinder multicastIpFinder() {
        return new TcpDiscoveryMulticastIpFinder().setMulticastGroup("228.1.2.8");
    }

    @Bean
    public TcpDiscoverySpi tcpDiscoverySpi(TcpDiscoveryIpFinder tcpDiscoveryIpFinder) {
        return new TcpDiscoverySpi()
                .setIpFinder(tcpDiscoveryIpFinder);
    }

    @Bean
    public IgniteConfiguration igniteConfiguration(TcpDiscoverySpi tcpDiscoverySpi) {
        // TODO: setup IgniteConfiguration. Use tcpDiscoverySpi as a discovery implementation. Plug configurations from statementCacheConfiguration() and usersCacheConfiguration(). Make this node run in client mode
        IgniteConfiguration igniteConfiguration = new IgniteConfiguration();
        igniteConfiguration.setPeerClassLoadingEnabled(true);
        CacheConfiguration cacheConfiguration = new CacheConfiguration();
        cacheConfiguration.setAtomicityMode(TRANSACTIONAL);
        statementCache(ignite(igniteConfiguration));
        userCache(ignite(igniteConfiguration));
        return igniteConfiguration.setDiscoverySpi(tcpDiscoverySpi);
    }

    @Bean
    public Ignite ignite(IgniteConfiguration igniteConfiguration) {
        /*
         * TODO: Create Ignite instance and start it
         * TODO v2: I've already configured native persistence, but cluster is not active? Add some code here to connect to Ignite.
         */
        igniteConfiguration.setClientMode(true);
        Ignite ignite = Ignition.getOrStart(igniteConfiguration);
        ignite.active(true);
        return ignite;
    }

    @Bean
    public CacheConfiguration statementCacheConfiguration() {
        // TODO: setup statement cache configuration. You can take cache name from properties.getStatement().getName()
        CacheConfiguration cacheConfiguration = new CacheConfiguration(properties.getStatement().getName());
        return cacheConfiguration;
    }

    @Bean
    public CacheConfiguration<String, User> usersCacheConfiguration() {
        // TODO: setup users cache configuration. You can take cache name from properties.getUsers().getName()
        CacheConfiguration<String, User> cacheConfiguration = new CacheConfiguration<>(properties.getStatement().getName());
        return cacheConfiguration;
    }

    @Bean
    public IgniteCache<String, Statement> statementCache(Ignite ignite) {
        // TODO: create statement cache instance bean if not exists, or use existent one. Use cache name from properties.getStatement().getName()
        return ignite.getOrCreateCache(properties.getStatement().getName());
    }

    @Bean
    public IgniteCache<String, User> userCache(Ignite ignite) {
        // TODO: create user cache instance bean if not exists, or use existent one. Use cache name from properties.getUsers().getName()
        return ignite.getOrCreateCache(properties.getUsers().getName());
    }
}
