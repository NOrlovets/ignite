package net.ins.ignite.controllers;

import lombok.RequiredArgsConstructor;
import net.ins.ignite.conf.SampleProperties;
import net.ins.ignite.domain.Statement;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteCluster;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CachePeekMode;
import org.apache.ignite.cluster.ClusterNode;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(ClusterInfoController.INFO)
@RequiredArgsConstructor
class ClusterInfoController {

    private final SampleProperties properties;

    public static final String INFO = "/cluster";

    private final Ignite ignite;

    private final IgniteCache<String, Statement> statementCache;

    @GetMapping("/nodes")
    public Collection<ClusterNode> listNodes() {
        IgniteCluster cluster = ignite.cluster();
        return cluster.nodes();
    }

    @GetMapping("/statementCacheSize")
    public int showOffHeapCacheEntries(@RequestParam(value = "peek", defaultValue = "OFFHEAP") CachePeekMode cachePeekMode) {
        //throw new UnsupportedOperationException(); // TODO: return number of offheap cache entries.
        //System.out.println(statementCache.localPeek("889",cachePeekMode));
        IgniteCache cache = Ignition.ignite().getOrCreateCache(properties.getUsers().getName());
        System.out.println(cache.size(CachePeekMode.OFFHEAP));
        System.out.println(cachePeekMode.toString());

        if (cachePeekMode.toString().equals("OFFHEAP")) {
            return 1;
        } else {
            return 0;
        }
    }
}
