package net.ins.ignite.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.ins.ignite.conf.IgniteConfig;
import net.ins.ignite.domain.Statement;
import net.ins.ignite.domain.Transaction;
import net.ins.ignite.domain.User;
import net.ins.ignite.exceptions.NoStatementFoundException;
import net.ins.ignite.exceptions.NoUserFoundException;
import net.ins.ignite.exceptions.UserAlreadyExistsException;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteTransactions;
import org.apache.ignite.Ignition;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.TransactionIsolation;
import org.apache.ignite.transactions.TransactionOptimisticException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.time.ZonedDateTime.now;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static net.ins.ignite.controllers.SampleIgniteController.ROOT_URI;
import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.CREATED;

/**
 * @author Ins137@gmail.com
 * Created at 8/18/2017
 * <p>
 * Don't ever add logic into controller.
 */
@RestController
@RequestMapping(ROOT_URI)
@Slf4j
@RequiredArgsConstructor
public class SampleIgniteController {

    @Autowired
    IgniteConfig igniteConfig;

    static final String ROOT_URI = "/ignite";
    static final String STATEMENT_URI = "/statement/{id}";
    static final String USER_URI = "/user";
    static final String USER_ID_URI = SampleIgniteController.USER_URI + "/{id}";

    private final IgniteCache<String, Statement> statementCache;
    private final IgniteCache<String, User> userCache;
    private final Ignite ignite;

    @GetMapping(STATEMENT_URI)
    public Statement getStatement(@PathVariable("id") String id) {
        return ofNullable(statementCache.get(id))
                .orElseThrow(() -> new NoStatementFoundException(id));
    }

    @GetMapping(USER_ID_URI)
    public User getUser(@PathVariable("id") String id) {
        return ofNullable(userCache.get(id))
                .orElseThrow(() -> new NoUserFoundException(id));
    }

    @GetMapping(USER_URI)
    public List<User> findUsersByLastName(@RequestParam("lastName") String lastName) {
        return emptyList(); // TODO: find users by lastName using SqlQuery.
    }

        //TODO: classic
//    @PutMapping(STATEMENT_URI)
//    @ResponseStatus(ACCEPTED)
//    public void updateStatement(@PathVariable("id") String id, @RequestBody List<Transaction> transactions) {
//        Statement statement = statementCache.get(id);
//        statement = ofNullable(statement)
//                .orElseGet(Statement::new);
//
//        statement
//                .setUserId(id)
//                .setVersion(statement.getVersion() + 1)
//                .setTimestamp(now())
//                .getTransactions()
//                .addAll(transactions);
//
//        statementCache.put(id, statement);
//    }

    //TODO: 3.b task
//    @PutMapping(STATEMENT_URI)
//    @ResponseStatus(ACCEPTED)
//    public void updateStatement(@PathVariable("id") String id, @RequestBody List<Transaction> transactions) {
//        Ignite ignite = Ignition.ignite();
//        try(org.apache.ignite.transactions.Transaction tx = ignite.transactions().txStart(TransactionConcurrency.OPTIMISTIC,
//                TransactionIsolation.SERIALIZABLE)) {
//            Statement statement = statementCache.get(id);
//            statement = ofNullable(statement)
//                    .orElseGet(Statement::new);
//
//            statement
//                    .setUserId(id)
//                    .setVersion(statement.getVersion() + 1)
//                    .setTimestamp(now())
//                    .getTransactions()
//                    .addAll(transactions);
//
//            statementCache.put(id, statement);
//
//        tx.commit();
//        }
//        catch (TransactionOptimisticException e){
//            log.error("Rollback bcs lose trans conflict");
//        }
//    }

    //TODO: 3.a task
    @PutMapping(STATEMENT_URI)
    @ResponseStatus(ACCEPTED)
    public void updateStatement(@PathVariable("id") String id, @RequestBody List<Transaction> transactions) {
        Ignite ignite = Ignition.ignite();
        try(org.apache.ignite.transactions.Transaction tx = ignite.transactions().txStart(TransactionConcurrency.PESSIMISTIC,
                TransactionIsolation.SERIALIZABLE)) {
            Statement statement = statementCache.get(id);
            statement = ofNullable(statement)
                    .orElseGet(Statement::new);

            statement
                    .setUserId(id)
                    .setVersion(statement.getVersion() + 1)
                    .setTimestamp(now())
                    .getTransactions()
                    .addAll(transactions);

            statementCache.put(id, statement);

            tx.commit();
        }
    }

    @PutMapping(USER_URI)
    @ResponseStatus(CREATED)
    public void createUser(@RequestBody User user) {
        if (!userCache.putIfAbsent(user.getId(), user)) {
            throw new UserAlreadyExistsException(user.getId());
        }
    }


    @DeleteMapping(STATEMENT_URI)
    public void deleteStatement(@PathVariable("id") String id) {
        statementCache.clear(id);
    }

    @DeleteMapping(USER_ID_URI)
    public void deleteUser(@PathVariable("id") String id) {
        userCache.clear(id);
    }
}
