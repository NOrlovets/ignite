package net.ins.ignite.domain;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
public class Transaction implements Serializable {
    private String srcAccount;
    private String dstAccount;
    private BigDecimal amount;
    private ZonedDateTime dateTime;
}
