package net.ins.ignite.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ignite.cache.affinity.AffinityKeyMapped;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    @AffinityKeyMapped
    private String id;
    // TODO: some of fields must be annotated to be utilized in SQL queries
    private String firstName;
    private String lastName;
    private String middleName;
    private String email;
}
