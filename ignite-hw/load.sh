#!/bin/bash

for ((i=0;i<1000;i++))
do
KEY=$(printf %05d $i)
echo "Adding entry for key: $KEY"
curl -X PUT -H "Content-Type: application/json" -d '[
  {
    "srcAccount": "12345678901234567890",
    "dstAccount": "09876543210987654321",
    "amount": 9999999.99,
    "dateTime": "2017-07-01T00:00:00+03:00"
  }
]' "http://localhost:8081/ignite/statement/$KEY"
done
